package actors

import actors.ManagerActor.{Check, TimePassed}
import akka.actor.{Actor, ActorRef, ActorRefFactory, Props}
import dto.Sla

import scala.collection.mutable

object ManagerActor {

  def props(graceRps: Int, createWorkerFunc: (Int, String) => ActorRefFactory => ActorRef = defaultWorkerCreator) =
    Props(classOf[ManagerActor], graceRps, createWorkerFunc)

  def defaultWorkerCreator(rps: Int, name: String): ActorRefFactory => ActorRef =
    (context: ActorRefFactory) => context.actorOf(RequestCounterActor.props(rps), name)

  case class Check(userSlaOpt: Option[Sla])

  case class TimePassed(partOfTheSecond: Int)
}

class ManagerActor(graceRps: Int, createWorkerFunc: (Int, String) => ActorRefFactory => ActorRef) extends Actor {
  private val existWorkers = mutable.Map[String, ActorRef]()
  private val guestWorker = createWorkerFunc(graceRps, "guestWorker")(context)

  override def receive: Receive = {
    case Check(userSlaOpt) =>
      userSlaOpt match {
        case Some(userSla) =>
          existWorkers.get(userSla.user) match {
            case Some(worker) =>
              worker forward RequestCounterActor.Request
            case None =>
              val newWorker = createWorkerFunc(userSla.rps, s"userWorker@${userSla.user}")(context)
              existWorkers += (userSla.user -> newWorker)
              newWorker forward RequestCounterActor.Request
          }
        case None => guestWorker forward RequestCounterActor.Request
      }

    case tp: TimePassed =>
      guestWorker ! tp
      existWorkers.values.foreach(_ ! tp)
  }
}
