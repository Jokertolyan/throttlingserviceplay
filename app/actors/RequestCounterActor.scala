package actors

import actors.ManagerActor.TimePassed
import akka.actor.{Actor, Props}

object RequestCounterActor {

  case object Request

  def props(rps: Int) = Props(classOf[RequestCounterActor], rps)
}

// TODO: think about killing an actor which is in idle state too long
// such behaviour depends on load nature
class RequestCounterActor(rps: Int) extends Actor {

  import actors.RequestCounterActor.Request

  private var actionsPerRequest = 1
  private var actionPoints = rps

  override def receive: Receive = {
    case Request =>
      if (actionPoints >= actionsPerRequest) {
        actionPoints -= actionsPerRequest
        sender() ! true
      } else {
        sender() ! false
      }

    case TimePassed(partOfTheSecond: Int) =>
      // update actionsPerRequest and recalculate current count of actionsPoints according to new actionsPerRequest value
      if (actionsPerRequest != partOfTheSecond) {
        actionPoints = (actionPoints / actionsPerRequest) * partOfTheSecond
        actionsPerRequest = partOfTheSecond
      }

      // increase amount of action points
      actionPoints += rps // (* actionsPerRequest / partOfTheSecond) this part is always 1, so we can omit it

      // don't allow to accumulate actionPoints during long intervals of inactivity
      actionPoints = Math.min(actionPoints, rps * actionsPerRequest)
  }
}
