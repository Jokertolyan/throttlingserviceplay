package services

import javax.inject.Inject

import actors.ManagerActor
import akka.actor.ActorSystem
import akka.pattern._
import akka.util.Timeout
import dto.Sla
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.language.postfixOps

trait ThrottlingService {
  // configurable
  val graceRps: Int

  // use mocks/stubs for testing
  val slaService: SlaService

  // Should return true if the request is within allowed RPS.
  def isRequestAllowed(token: Option[String]): Boolean
}

class ThrottlingServiceImpl @Inject()(
                                       system: ActorSystem,
                                       val graceRps: Int,
                                       val slaService: SlaService) extends ThrottlingService {
  // this Map can be replaced by some cache engine, but such decision should be made according to environment
  private val cache = new java.util.concurrent.ConcurrentHashMap[String, Future[Sla]]
  private[services] val managerActor = system.actorOf(ManagerActor.props(graceRps))
  private val actorAnswerTimeout = 5 millis
  implicit private val actorAskTimeout = Timeout(actorAnswerTimeout)

  private[services] def startScheduler(): Unit = {
    system.scheduler.schedule(0 millis, 100 millis, managerActor, ManagerActor.TimePassed(10))
  }

  startScheduler()

  def isRequestAllowed(token: Option[String]): Boolean = {
    val resultFuture = managerActor ? ManagerActor.Check(getUserSla(token))

    // Await.result timeout should be any value bigger than actor answer timeout
    // In this case we will never get TimeoutException, and the maximum delay is controlled by actor ask method
    // but the best solution is to change return type of the method to Future[Boolean]
    Await.result(resultFuture.mapTo[Boolean].fallbackTo(Future.successful(false)), actorAnswerTimeout plus (1 second))
  }

  private[services] def getUserSla(tokenOpt: Option[String]): Option[Sla] = {
    tokenOpt flatMap { token =>
      Option(cache.get(token)) match {
        // known user
        case Some(slaFuture) if slaFuture.isCompleted => slaFuture.value.flatMap(_.toOption)
        // already sent request to SlaService
        case Some(_) => None
        // unknown user
        case None =>
          cache.put(token, slaService.getSlaByToken(token))
          None
      }
    }
  }
}
