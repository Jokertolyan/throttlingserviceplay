package services

import dto.Sla

import scala.concurrent.Future

trait SlaService {
  def getSlaByToken(token: String): Future[Sla]
}
