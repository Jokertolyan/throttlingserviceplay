package load_testing

import akka.actor.{ActorSystem, Props}
import akka.pattern.ask
import akka.testkit.TestKit
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import dto.Sla
import load_testing.Actors._
import org.scalatest.WordSpecLike
import services.{SlaService, ThrottlingServiceImpl}
import utils.AkkaConfig

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.language.postfixOps
import scala.util.Random

class LoadTesting extends TestKit(ActorSystem("ManagerActorTest", ConfigFactory.parseString(AkkaConfig.config)))
  with WordSpecLike {

  "A ThrottlingService" should {
    "process T*N*K request for N users, K rps during T seconds" in {

      // test params
      val N = 200L // amount of users
      val K = 250 // allowed rps
      val T = 120 // test time
      val measureErrorKoef = 0.95 // koef for comparing real amount of requests and expected

      // preparing service
      val slaServiceMock = new SlaService {
        override def getSlaByToken(token: String): Future[Sla] = Future {
          Thread.sleep(200 + Random.nextInt(101))
          Sla(token, K)
        }
      }

      val service = new ThrottlingServiceImpl(system, K, slaServiceMock)

      val askManager = system.actorOf(Props(classOf[AskManagerActor], (1L to N).map(_.toString), K, service, T))

      // simulation
      askManager ! StartNewSimulation

      Thread.sleep(T * 1000)

      askManager ! EndSimulation

      implicit val resultTimeout = Timeout(5 seconds)

      val resultFuture = (askManager ? GetResults).mapTo[Long]

      val result = Await.result(resultFuture, resultTimeout.duration)

      if (result < (T * N * K) * measureErrorKoef) {
        fail(s"Expected: ${T * N * K}. Actual: $result")
      } else {
        info(s"Expected: ${T * N * K}. Actual: $result")
      }
    }
  }
}
