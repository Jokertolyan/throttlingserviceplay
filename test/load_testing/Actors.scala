package load_testing

import akka.actor.{Actor, Props}
import load_testing.Actors._
import services.ThrottlingService

object Actors {

  case class Result(successfulRequests: Long)

  case object StartNewSimulation

  case object EndSimulation

  case object SendRequest

  case object GetResults

}

class AskActor(token: Option[String], rps: Int, throttlingService: ThrottlingService) extends Actor {

  var successfulRequests = 0L

  override def receive: Receive = {
    case StartNewSimulation =>
      successfulRequests = 0
      context.become(simulation)
      self ! SendRequest
  }

  def simulation: Receive = {
    case SendRequest =>
      if (throttlingService.isRequestAllowed(token)) successfulRequests += 1
      self ! SendRequest

    case EndSimulation =>
      context.parent ! Result(successfulRequests)
      context.unbecome()
  }
}

class AskManagerActor(
                       tokens: Seq[String],
                       rps: Int,
                       throttlingService: ThrottlingService,
                       durationSeconds: Int) extends Actor {
  private val askers = tokens map { token =>
    context.actorOf(Props(classOf[AskActor], Some(token), rps, throttlingService))
  }
  private var successfullRequests = 0L
  private var resultsCollected = 0

  override def receive: Receive = readyForNewSimulation

  def readyForNewSimulation: Receive = {
    case StartNewSimulation =>
      context.become(simulationInProcess)
      askers foreach {
        _ ! StartNewSimulation
      }

    case GetResults =>
      sender() ! successfullRequests
  }

  def simulationInProcess: Receive = {
    case EndSimulation =>
      context.become(collectingResults)
      resultsCollected = 0
      askers foreach {
        _ ! EndSimulation
      }
    case other => self forward other
  }

  def collectingResults: Receive = {
    case Result(askerSuccessfulRequests) =>
      resultsCollected += 1
      successfullRequests += askerSuccessfulRequests
      if (resultsCollected == tokens.length) {
        context.become(readyForNewSimulation)
      }
    case other => self forward other
  }
}
