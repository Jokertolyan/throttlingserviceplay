package actors

import actors.ManagerActor.TimePassed
import actors.RequestCounterActor.Request
import akka.actor.ActorSystem
import akka.testkit.{DefaultTimeout, ImplicitSender, TestKit}
import com.typesafe.config.ConfigFactory
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}
import utils.AkkaConfig

import scala.language.postfixOps

class RequestCounterActorTest
  extends TestKit(ActorSystem("RequestCounterActorTest", ConfigFactory.parseString(AkkaConfig.config)))
    with WordSpecLike
    with BeforeAndAfterAll
    with DefaultTimeout
    with ImplicitSender {

  override def afterAll {
    shutdown()
  }

  "A RequestCounterActorTest" should {
    "allow requests if there are enough actionPoints" in {
      val testActor = system.actorOf(RequestCounterActor.props(10))
      testActor ! Request
      expectMsg(true)
    }

    "don't allow requests if there are not enough actionPoints" in {
      val testActor = system.actorOf(RequestCounterActor.props(0))
      testActor ! Request
      expectMsg(false)
    }

    "increase amount of actionPoints on 'TimePassed' message" in {
      val testActor = system.actorOf(RequestCounterActor.props(2))
      // spent initial request points
      testActor ! Request
      expectMsg(true)
      testActor ! Request
      expectMsg(true)
      testActor ! Request
      expectMsg(false)

      // add point for 1/2 of the second: 2/2 = 1
      testActor ! TimePassed(2)

      testActor ! Request
      expectMsg(true)
      testActor ! Request
      expectMsg(false)
    }

    "don't accumulate actionPoints more than for RPS" in {
      val testActor = system.actorOf(RequestCounterActor.props(2))

      // try to add some actionPoints to already full actor
      testActor ! TimePassed(1)

      // spent initial request points
      testActor ! Request
      expectMsg(true)
      testActor ! Request
      expectMsg(true)

      //check that there are no more action points
      testActor ! Request
      expectMsg(false)
    }
  }
}
