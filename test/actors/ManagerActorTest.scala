package actors

import akka.actor.{ActorRef, ActorRefFactory, ActorSystem}
import akka.testkit.{DefaultTimeout, ImplicitSender, TestKit, TestProbe}
import com.typesafe.config.ConfigFactory
import dto.Sla
import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfterAll, MustMatchers, WordSpecLike}
import utils.AkkaConfig

import scala.concurrent.duration._
import scala.language.postfixOps

class ManagerActorTest extends TestKit(ActorSystem("ManagerActorTest", ConfigFactory.parseString(AkkaConfig.config)))
  with WordSpecLike
  with BeforeAndAfterAll
  with DefaultTimeout
  with ImplicitSender
  with MockFactory
  with MustMatchers {

  override def afterAll {
    shutdown()
  }

  "A ManagerActor" should {
    "forward all requests from unknown users to guestWorker" in {
      val createWorkerFunction = stubFunction[ActorRef]

      def createWorkerFunctionWrapper(rps: Int, name: String)(context: ActorRefFactory) = createWorkerFunction()

      val guestWorker = TestProbe()
      val authorizedWorker = TestProbe()

      createWorkerFunction.when().returns(guestWorker.ref).once()
      createWorkerFunction.when().returns(authorizedWorker.ref)

      val testedActor = system.actorOf(ManagerActor.props(10, createWorkerFunctionWrapper))

      testedActor ! ManagerActor.Check(None)
      guestWorker.expectMsg(RequestCounterActor.Request)
      authorizedWorker.expectNoMsg(500 millis)
      guestWorker.lastSender mustEqual testActor

      createWorkerFunction.verify().once()
    }

    "create new worker for each new authorized user and forward message to it" in {
      val createWorkerFunction = mockFunction[ActorRef]

      def createWorkerFunctionWrapper(rps: Int, name: String)(context: ActorRefFactory) = createWorkerFunction()

      val guestWorker = TestProbe()
      val authorizedWorker1 = TestProbe()
      val authorizedWorker2 = TestProbe()

      createWorkerFunction.expects().returning(guestWorker.ref).once()
      createWorkerFunction.expects().returning(authorizedWorker1.ref).once()
      createWorkerFunction.expects().returning(authorizedWorker2.ref).once()

      val testedActor = system.actorOf(ManagerActor.props(10, createWorkerFunctionWrapper))

      testedActor ! ManagerActor.Check(Some(Sla("1", 100)))
      authorizedWorker1.expectMsg(RequestCounterActor.Request)
      testedActor ! ManagerActor.Check(Some(Sla("2", 100)))
      authorizedWorker2.expectMsg(RequestCounterActor.Request)
      guestWorker.expectNoMsg(500 millis)
      authorizedWorker1.lastSender mustEqual testActor
      authorizedWorker2.lastSender mustEqual testActor
    }

    "do not create new worker if there are already a worker for the user" in {
      val createWorkerFunction = mockFunction[ActorRef]

      def createWorkerFunctionWrapper(rps: Int, name: String)(context: ActorRefFactory) = createWorkerFunction()

      val guestWorker = TestProbe()
      val authorizedWorker = TestProbe()

      createWorkerFunction.expects().returning(guestWorker.ref).once()
      createWorkerFunction.expects().returning(authorizedWorker.ref).once()

      val testedActor = system.actorOf(ManagerActor.props(10, createWorkerFunctionWrapper))

      testedActor ! ManagerActor.Check(Some(Sla("1", 100)))
      authorizedWorker.expectMsg(RequestCounterActor.Request)
      testedActor ! ManagerActor.Check(Some(Sla("1", 100)))
      authorizedWorker.expectMsg(RequestCounterActor.Request)
      guestWorker.expectNoMsg(500 millis)
    }

    "resend TimePassed message to all workers" in {
      val createWorkerFunction = mockFunction[ActorRef]

      def createWorkerFunctionWrapper(rps: Int, name: String)(context: ActorRefFactory) = createWorkerFunction()

      val guestWorker = TestProbe()
      val authorizedWorker1 = TestProbe()
      val authorizedWorker2 = TestProbe()

      createWorkerFunction.expects().returning(guestWorker.ref).once()
      createWorkerFunction.expects().returning(authorizedWorker1.ref).once()
      createWorkerFunction.expects().returning(authorizedWorker2.ref).once()

      val testedActor = system.actorOf(ManagerActor.props(10, createWorkerFunctionWrapper))
      testedActor ! ManagerActor.Check(Some(Sla("1", 100)))
      testedActor ! ManagerActor.Check(Some(Sla("2", 100)))

      val message = ManagerActor.TimePassed(1)

      testedActor ! message
      guestWorker.expectMsg(message)
      authorizedWorker1.expectMsg(RequestCounterActor.Request)
      authorizedWorker1.expectMsg(message)
      authorizedWorker2.expectMsg(RequestCounterActor.Request)
      authorizedWorker2.expectMsg(message)
    }
  }
}
