package services

import actors.ManagerActor
import akka.actor.{ActorRef, ActorSystem, Cancellable}
import akka.testkit.{DefaultTimeout, ImplicitSender, TestKit, TestProbe}
import com.typesafe.config.ConfigFactory
import dto.Sla
import org.scalamock.scalatest.MockFactory
import org.scalatest._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Seconds, Span}
import utils.AkkaConfig

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Future, Promise}
import scala.language.postfixOps
import scala.util.Success

class ThrottlingServiceImplTest extends TestKit(ActorSystem("ManagerActorTest", ConfigFactory.parseString(AkkaConfig.config)))
  with WordSpecLike
  with BeforeAndAfterAll
  with DefaultTimeout
  with ImplicitSender
  with MockFactory
  with MustMatchers
  with ScalaFutures {

  trait Fixture {
    val slaService = mock[SlaService]
    val managerActorStub = TestProbe()
    val service = new ThrottlingServiceImpl(system, 100, slaService) {
      override private[services] val managerActor: ActorRef = managerActorStub.ref

      override private[services] def startScheduler(): Unit = () // do nothing
    }
  }

  "A ThrottlingServiceImpl.getUserSla" should {
    "return None if there are no token" in new Fixture {
      service.getUserSla(None) mustEqual None
    }

    "return None if user is unknown" in new Fixture {
      val token = "token"
      (slaService.getSlaByToken _).expects(token).returns(Future.successful(Sla("1", 100)))

      service.getUserSla(Some(token)) mustEqual None
    }

    "return None if request to SlaService is not finished yet and don't call " in new Fixture {
      val token = "token"
      val requestPromise = Promise[Sla]()
      (slaService.getSlaByToken _).expects(token).returns(requestPromise.future)

      service.getUserSla(Some(token))
      service.getUserSla(Some(token)) mustEqual None
      requestPromise.complete(Success(Sla("1", 100)))
    }

    "return Some if user is already known" in new Fixture {
      val token = "token"
      val sla = Sla("1", 100)
      (slaService.getSlaByToken _).expects(token).returns(Future.successful(sla))

      service.getUserSla(Some(token))
      service.getUserSla(Some(token)) mustEqual Some(sla)
    }
  }

  "A ThrottlingServiceImpl.isRequestAllowed" should {
    "get user SLA and send ask request to manager actor" in new Fixture {
      val actorAnswer = true

      val result = Future(service.isRequestAllowed(None))

      managerActorStub.expectMsg(500 millis, ManagerActor.Check(None))
      managerActorStub.reply(actorAnswer)

      result.futureValue mustEqual actorAnswer
    }

    "return false if actor not responding too long(> 1 second)" in new Fixture {
      val result = Future(service.isRequestAllowed(None))

      result.futureValue(PatienceConfig(Span(2, Seconds))) mustEqual false
    }
  }
}
